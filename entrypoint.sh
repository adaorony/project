#!/bin/bash

composer install \

cp .env.example .env

php artisan key:g \

update-alternatives --set php /usr/bin/php7.2

a2dismod mpm_event
a2enmod php7.2

sed -i "s/VirtualHost \*:80/VirtualHost \*:$PORT/g" /etc/apache2/sites-enabled/000-default.conf
sed -i "s/Listen 80/Listen $PORT/g" /etc/apache2/ports.conf

service apache2 restart

ln -sfn /app/public /var/www/dev

service apache2 start

tail -f /tmp/dev.log
